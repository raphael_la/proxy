﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Proxy_DLL;

namespace Proxy
{
    public partial class Form1 : Form
    {
        WifiProxy w;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            w = new WifiProxy();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.pictureBox1.Image = w.GetMicrosoftImage();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.pictureBox1.Image = w.GetAppleImage();
        }
    }
}
