﻿using ProxyInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Proxy_DLL;

namespace Proxy
{
    public class WifiProxy : IWifi
    {
        Wifi w;

        public WifiProxy() { }


        public Image GetAppleImage()
        {
            if (w == null)
                w = new Wifi();


            return w.GetAppleImage();
        }

        public Image GetMicrosoftImage()
        {
            if (w == null)
                w = new Wifi();

            return w.GetMicrosoftImage();
        }
    }
}
